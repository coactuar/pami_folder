-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 02:52 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pami100421`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'banglorre', NULL, NULL, '2021/04/08 13:29:33', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'banglorre', NULL, NULL, '2021/04/08 13:29:33', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'banglorre', NULL, NULL, '2021/04/08 13:55:57', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(4, 'Reynold', 'reynold@coact.co.in', 'coact', 'coact', NULL, NULL, '2021/04/08 14:48:06', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(5, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'coact', NULL, NULL, '2021/04/08 15:16:08', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(6, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/04/08 15:48:34', '2021-04-08', '2021-04-08', 1, 'Abbott'),
(7, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021/04/09 20:13:51', '2021-04-09', '2021-04-09', 1, 'Abbott'),
(8, 'SYED AKBARUL HAQUE', 'akbar_ul@hotmail.com', 'mumbai', 'criticare', NULL, NULL, '2021/04/09 20:15:26', '2021-04-09', '2021-04-09', 1, 'Abbott'),
(9, 'sunil wani', 'zapsunil1@gmail.com', 'mumbai', 'kdah', NULL, NULL, '2021/04/09 20:17:21', '2021-04-09', '2021-04-09', 1, 'Abbott'),
(10, 'A', 'a@m', 'A', 'A', NULL, NULL, '2021/04/10 09:07:27', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(11, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021/04/10 17:35:14', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(12, 'DR ABHISHEK WADKAR', 'drabhicardio1@gmail.com', 'Mumbai', 'SURANA ', NULL, NULL, '2021/04/10 17:38:55', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(13, 'DR ABHISHEK WADKAR', 'drabhicardio1@gmail.com', 'Mumbai', 'SURANA ', NULL, NULL, '2021/04/10 17:40:26', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(14, 'Mandakini Bhatia', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021/04/10 17:43:08', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(15, 'DR ABHISHEK WADKAR', 'drabhicardio1@gmail.com', 'Mumbai', 'SURANA ', NULL, NULL, '2021/04/10 17:45:39', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(16, 'VIJAYSINH', 'dr.vijaysinh@gmail.com', 'nashik', 'wockhardt', NULL, NULL, '2021/04/10 17:48:03', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(17, 'SYED AKBARUL HAQUE', 'akbar_ul@hotmail.com', 'mumbai', 'criticare', NULL, NULL, '2021/04/10 17:50:49', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(18, 'VIJAYSINH PATIL', 'dr.vijaysinh@gmail.com', 'NASHIK', 'WOCKHARDT', NULL, NULL, '2021/04/10 17:53:39', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(19, 'VIJAYSINH PATIL', 'dr.vijaysinh@gmail.com', 'NASHIK', 'WOCKHARDT', NULL, NULL, '2021/04/10 17:53:39', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(20, 'sunil wani', 'zapsunil@hotmail.com', 'mumbai', 'kdah', NULL, NULL, '2021/04/10 17:57:20', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(21, 'Dr Ajaykumar Mahaj', 'draumahajan@gmail.com', 'M', 'LTMG ', NULL, NULL, '2021/04/10 18:05:04', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(22, 'Dr Ajaykumar Mahajan ', 'draumahajan@gmail.com', 'Mumbai ', 'LTMG ', NULL, NULL, '2021/04/10 18:06:30', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(23, 'Anup ', 'anuprtaksande@gmail.com', 'Mumbai', 'Wockhardt ', NULL, NULL, '2021/04/10 18:10:27', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(24, 'anup ta', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', NULL, NULL, '2021/04/10 18:18:56', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(25, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021/04/10 18:32:57', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(26, 'anup taksande', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', NULL, NULL, '2021/04/10 19:15:03', '2021-04-10', '2021-04-10', 1, 'Abbott'),
(27, 'Anup', 'anuprtaksande@gmail.com', 'Mumbai', 'Wockhardt ', NULL, NULL, '2021/04/10 19:35:14', '2021-04-10', '2021-04-10', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_new`
--

INSERT INTO `faculty_new` (`id`, `Name`, `EmailID`, `City`, `Hospital`, `Last Login On`, `Logout Times`) VALUES
(2, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', '10-04-2021 17:44:20\n', '10-04-2021 20:03:40'),
(3, 'SYED AKBARUL HAQUE', 'akbar_ul@hotmail.com', 'mumbai', 'criticare', '10-04-2021 17:51:17', '10-04-2021 20:30:02'),
(4, 'sunil wani', 'zapsunil1@gmail.com', 'mumbai', 'kdah', '10-04-2021 17:57:51', '10-04-2021 17:59:46'),
(5, 'sunil wani', 'zapsunil1@gmail.com', 'mumbai', 'kdah', '10-04-2021 17:59:53', '10-04-2021 19:37:38'),
(6, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', '10-04-2021 17:35:02', '10-04-2021 17:57:57'),
(7, 'DR ABHISHEK WADKAR', 'drabhicardio1@gmail.com', 'Mumbai', 'SURANA ', '10-04-2021 17:46:36', '10-04-2021 20:03:04'),
(8, 'VIJAYSINH PATIL ', 'dr.vijaysinh@gmail.com', 'NASHIK', 'WOCKHARDT', '10-04-2021 17:54:02', '10-04-2021 20:03:57'),
(9, 'Dr Ajaykumar Mahaj', 'draumahajan@gmail.com', 'M', 'LTMG ', '10-04-2021 18:07:52', '10-04-2021 20:03:16'),
(10, 'Anup ', 'anuprtaksande@gmail.com', 'Mumbai', 'Wockhardt ', '10-04-2021 18:19:37', '10-04-2021 19:10:56'),
(11, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', '10-04-2021 18:33:28', '10-04-2021 20:03:49'),
(12, 'anup taksande', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', '10-04-2021 19:15:41', '10-04-2021 20:03:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'hello', '2021-04-07 12:43:03', 'Abbott', 0, 0),
(2, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Session?', '2021-04-10 18:09:44', 'Abbott', 0, 0),
(3, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'Dr. Wani\'s case - IVUS would help a lot.', '2021-04-10 18:25:47', 'Abbott', 0, 0),
(4, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'Dr. Wani\'s case - Which inotropes do you prefer in such case?', '2021-04-10 18:27:29', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-04-07 12:43:17', '2021-04-07 12:43:17', '2021-04-07 12:52:17', 1, 'Abbott'),
(2, 'neeraj', 'neeraj@coact.co.in', 'nellore', '12345678', NULL, NULL, '2021-04-07 12:50:46', '2021-04-07 12:50:46', '2021-04-07 12:52:17', 1, 'Abbott'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 16:14:28', '2021-04-07 16:14:28', '2021-04-07 16:14:58', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-04-07 12:42:56', '2021-04-07 12:42:56', '2021-04-07 12:43:05', 0, 'Abbott', 'ea35ea3b486d5dbd66038f9e374a5b12'),
(2, 'neeraj', 'neeraj@coact.co.in', 'neeraj', 'bng', NULL, NULL, '2021-04-07 12:50:24', '2021-04-07 12:50:24', '2021-04-07 12:50:29', 0, 'Abbott', 'd04f32b7caae3e9417304c3e98b6e3df'),
(3, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-04-07 14:21:08', '2021-04-07 14:21:08', '2021-04-07 14:21:21', 0, 'Abbott', '96b2b552c9516908f18744f7f84b48ba'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 14:23:39', '2021-04-07 14:23:39', '2021-04-07 14:23:47', 0, 'Abbott', '0e1ab372b94c35b8430e21554475988b'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 14:24:24', '2021-04-07 14:24:24', '2021-04-07 14:24:44', 0, 'Abbott', 'b1065dca40e48b9c8f0c22121daaef2e'),
(6, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 14:24:52', '2021-04-07 14:24:52', '2021-04-07 14:26:31', 0, 'Abbott', 'f1452022c4a2978eb718b28ea1a1fed7'),
(7, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 14:26:35', '2021-04-07 14:26:35', '2021-04-07 14:27:11', 0, 'Abbott', '5b8a68060a8db96541c75750deceea2c'),
(8, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-07 14:27:57', '2021-04-07 14:27:57', '2021-04-07 14:51:58', 0, 'Abbott', '1ddbc08bb88f9351e290629459ad9d8c'),
(9, 'Tanushree', 'tanushree@coact.co.in', 'bangalore', 'coact', NULL, NULL, '2021-04-07 14:29:01', '2021-04-07 14:29:01', '2021-04-07 15:10:23', 0, 'Abbott', '541806d215bd3ae0db8edab97b848e62'),
(10, 'Reynold', 'reynold@coact.co.in', 'coact', 'coact', NULL, NULL, '2021-04-07 14:29:53', '2021-04-07 14:29:53', '2021-04-07 14:42:44', 0, 'Abbott', 'c1284e4710a1f46806d8ed295674df01'),
(11, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-04-07 14:30:36', '2021-04-07 14:30:36', '2021-04-07 14:51:48', 0, 'Abbott', '79cfe954da36ad7e3cd480470fb21eb0'),
(12, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-04-07 14:33:50', '2021-04-07 14:33:50', '2021-04-07 15:12:07', 0, 'Abbott', 'a6799c991f529f75e1fd0cf8ea83bfa3'),
(13, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-07 14:53:02', '2021-04-07 14:53:02', '2021-04-07 14:54:44', 0, 'Abbott', '21e5a9b90e11594847a3bda8e2eddbb4'),
(14, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-07 15:03:15', '2021-04-07 15:03:15', '2021-04-07 15:03:19', 0, 'Abbott', '0c11750feb7ba0d3db837397bf061531'),
(15, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-07 15:22:20', '2021-04-07 15:22:20', '2021-04-07 15:22:33', 0, 'Abbott', '28a9189d8131b9301c8db18677802b78'),
(16, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-07 16:24:04', '2021-04-07 16:24:04', '2021-04-07 16:24:10', 0, 'Abbott', 'b35d0a118a2c10c2b7a80a175c571039'),
(17, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-08 13:26:19', '2021-04-08 13:26:19', '2021-04-08 13:26:23', 0, 'Abbott', 'c5fd1ef9bcdafceeaeb1c8d2bf9564fd'),
(18, 'Mandakini Bhatia', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-08 15:13:53', '2021-04-08 15:13:53', '2021-04-08 16:43:53', 0, 'Abbott', '3745a61f30af3f2f7bb12efa3ce406ac'),
(19, 'Mandakini Bhatia', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-08 15:13:53', '2021-04-08 15:13:53', '2021-04-08 16:43:53', 0, 'Abbott', '5a26a7f3e570864e636af148608407e7'),
(20, 'Mandakini Bhatia', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-08 15:14:12', '2021-04-08 15:14:12', '2021-04-08 16:44:12', 0, 'Abbott', '3ef7bc45a0569b25da97083a0eed98eb'),
(21, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'Bangalore', 'IT', NULL, NULL, '2021-04-08 15:20:02', '2021-04-08 15:20:02', '2021-04-08 15:20:06', 0, 'Abbott', 'd05ae1b63634e5070fa0c37cd35da1ba'),
(22, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'Bangalore', 'IT', NULL, NULL, '2021-04-08 15:20:35', '2021-04-08 15:20:35', '2021-04-08 16:50:35', 0, 'Abbott', 'dae4f551f518b15891723482b1bdf35a'),
(23, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-08 15:31:49', '2021-04-08 15:31:49', '2021-04-08 15:34:10', 0, 'Abbott', 'e7ff028108ec1bce6c609b538dc69f0d'),
(24, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-08 15:45:01', '2021-04-08 15:45:01', '2021-04-08 15:58:13', 0, 'Abbott', 'ccc426bea0354ddacd79d3be60782a81'),
(25, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-04-08 15:56:45', '2021-04-08 15:56:45', '2021-04-08 15:57:35', 0, 'Abbott', '9924c0b0e460f8e8df9f4a3aff67c003'),
(26, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-04-08 15:57:47', '2021-04-08 15:57:47', '2021-04-08 16:03:14', 0, 'Abbott', '1618d2c5b9c2c1dab3681d98a939487e'),
(27, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-04-08 15:58:06', '2021-04-08 15:58:06', '2021-04-08 15:59:03', 0, 'Abbott', 'c4ca17dba72fb5043c19e7f71bee3d30'),
(28, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'Bangalore', 'IT', NULL, NULL, '2021-04-08 15:59:47', '2021-04-08 15:59:47', '2021-04-08 16:00:47', 0, 'Abbott', '58458f6bc7978c97fa60aab92684fbd4'),
(29, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-04-08 16:04:40', '2021-04-08 16:04:40', '2021-04-08 16:05:03', 0, 'Abbott', '76b5f29c9ffb88bb82f9b6c92727a67f'),
(30, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-04-08 16:05:29', '2021-04-08 16:05:29', '2021-04-08 16:05:37', 0, 'Abbott', 'ff261801bc894730b2f359c6b059b239'),
(31, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-04-08 16:12:16', '2021-04-08 16:12:16', '2021-04-08 16:17:10', 0, 'Abbott', 'ad652f1abb3a67db88ad1fb4d6ff424f'),
(32, 'Sujatha', 'sujatha@coact.co.in', 'Bangalore', 'Bowring', NULL, NULL, '2021-04-08 16:16:59', '2021-04-08 16:16:59', '2021-04-08 16:17:18', 0, 'Abbott', 'bb2f5a89acdef54ec01ddfb3832849f5'),
(33, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-08 16:19:52', '2021-04-08 16:19:52', '2021-04-08 16:20:05', 0, 'Abbott', '3b4465f122f08e1ac1dfe05799048b9b'),
(34, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-09 14:42:12', '2021-04-09 14:42:12', '2021-04-09 14:42:19', 0, 'Abbott', '116c3d42101e82be0e601d625b7dc64d'),
(35, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-04-09 14:42:27', '2021-04-09 14:42:27', '2021-04-09 14:43:07', 0, 'Abbott', '3d5dfaaad67dd42b45c51fdecb31ab85'),
(36, 'Shrivallabh ', 'karlekar.shrivallabh@gmail.com', 'NANDED', 'Abhyuday ', NULL, NULL, '2021-04-10 11:17:28', '2021-04-10 11:17:28', '2021-04-10 11:17:39', 0, 'Abbott', '74eece7ac2296e09f68a666e71944e2d'),
(37, 'Ashish Agrawal', 'dr.agrawalashish@gmail.com', 'Mumbai ', 'USSH ', NULL, NULL, '2021-04-10 12:29:45', '2021-04-10 12:29:45', '2021-04-10 12:29:54', 0, 'Abbott', '0e78d88730364d9d6307dd26dd418f69'),
(38, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center ', NULL, NULL, '2021-04-10 17:10:25', '2021-04-10 17:10:25', '2021-04-10 17:10:36', 0, 'Abbott', '6224294712680aa7931405066f97ed6c'),
(39, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 17:11:33', '2021-04-10 17:11:33', '2021-04-10 17:11:40', 0, 'Abbott', 'b4c2288b54354977e758bffb7d9a07ca'),
(40, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-04-10 17:21:33', '2021-04-10 17:21:33', '2021-04-10 17:22:11', 0, 'Abbott', '93eb6566477fba469d5e0fe20728c57f'),
(41, 'Prashant suresh pawar', 'drprashant8087@gmail.com', 'Karad', 'Sahyadri hospital, karad', NULL, NULL, '2021-04-10 17:40:31', '2021-04-10 17:40:31', '2021-04-10 17:48:26', 0, 'Abbott', 'ca208d9a586e584951b95fadc5c180ba'),
(42, 'Badri vetal', 'badrivetal@gmail.com', 'Mumbai', 'Surana ', NULL, NULL, '2021-04-10 17:42:41', '2021-04-10 17:42:41', '2021-04-10 17:46:31', 0, 'Abbott', '2cad195f954dca3dc27badf830754e76'),
(43, 'Deepak', 'deepak.mondal@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 17:51:43', '2021-04-10 17:51:43', '2021-04-10 20:18:46', 0, 'Abbott', 'd7851dcd978320d2bc2376be1c3fbcb6'),
(44, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 17:58:30', '2021-04-10 17:58:30', '2021-04-10 18:01:22', 0, 'Abbott', '58967159d04c110d492cb42f07cfb45f'),
(45, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 17:59:41', '2021-04-10 17:59:41', '2021-04-10 18:04:41', 0, 'Abbott', '8159e085c3d20c8ed05ca95782fbeea0'),
(46, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 18:00:57', '2021-04-10 18:00:57', '2021-04-10 18:20:58', 0, 'Abbott', '4656bca0f32b14d268efdfd7f438a904'),
(47, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:01:01', '2021-04-10 18:01:01', '2021-04-10 18:03:14', 0, 'Abbott', '45ca58958640b0711df400de94bed31a'),
(48, 'Dr Yunus loya', 'yunusloya@gmail.com', 'MUMBAI', 'Prince aly khan hospital', NULL, NULL, '2021-04-10 18:02:51', '2021-04-10 18:02:51', '2021-04-10 18:03:03', 0, 'Abbott', '35e54556e9e24d9678949515d2a782d3'),
(49, 'Rahul Mahadik', 'rjmahadik@gmail.com', 'Sangli', 'Kulloli Hospital Sanglu', NULL, NULL, '2021-04-10 18:03:02', '2021-04-10 18:03:02', '2021-04-10 18:06:42', 0, 'Abbott', '5433a6ad4a369803ef953dc414be1b95'),
(50, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:03:10', '2021-04-10 18:03:10', '2021-04-10 18:03:41', 0, 'Abbott', '0ac77a8c76058491614cb5ea3f467d4f'),
(51, 'Joel', 'joelquadros11@gmail.com', 'Margao', 'Gmc', NULL, NULL, '2021-04-10 18:03:29', '2021-04-10 18:03:29', '2021-04-10 18:04:46', 0, 'Abbott', '4ef934a14a2d7d9c688c1016eb5b7b14'),
(52, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:03:46', '2021-04-10 18:03:46', '2021-04-10 18:06:18', 0, 'Abbott', 'fdf5494ce472412b62069ef6ec717582'),
(53, 'Dr Nazir juvale', 'drjuvale@rediffmail.com', 'MUMBAI', 'Saifee hospital', NULL, NULL, '2021-04-10 18:04:11', '2021-04-10 18:04:11', '2021-04-10 18:04:16', 0, 'Abbott', '375c53a8cc0ac0983938c5df08b38987'),
(54, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:04:53', '2021-04-10 18:04:53', '2021-04-10 18:06:43', 0, 'Abbott', 'e30929e6561e9d1a4006c727ba3a89f9'),
(55, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:05:02', '2021-04-10 18:05:02', '2021-04-10 18:05:30', 0, 'Abbott', '780f1902f1e2705baa4a12296ed7e9e4'),
(56, 'Dr Nazir juvale', 'drjuvale@rediffmail.com', 'MUMBAI', 'Saifee hospital', NULL, NULL, '2021-04-10 18:05:10', '2021-04-10 18:05:10', '2021-04-10 18:05:19', 0, 'Abbott', '99472bf8d578ea8c4cb540a0b6391f84'),
(57, 'Dr Yunus loya', 'yunusloya@gmail.com', 'MUMBAI', 'Prince aly khan hospital', NULL, NULL, '2021-04-10 18:05:21', '2021-04-10 18:05:21', '2021-04-10 18:05:28', 0, 'Abbott', 'd40609d41a34b64f42d40e4cebd81d4a'),
(58, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:05:29', '2021-04-10 18:05:29', '2021-04-10 18:05:54', 0, 'Abbott', 'ea457fa6ffa26c6c5bf620e4f96a687f'),
(59, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:05:48', '2021-04-10 18:05:48', '2021-04-10 18:08:30', 0, 'Abbott', '99de9dce27e4a3e18c48010e981de5cc'),
(60, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 18:06:59', '2021-04-10 18:06:59', '2021-04-10 18:09:30', 0, 'Abbott', '26e5cb3141a0db6fa86853315de705e4'),
(61, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:07:16', '2021-04-10 18:07:16', '2021-04-10 18:10:08', 0, 'Abbott', '810146bd2ae1f2b430809b9ba7f5a299'),
(62, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:07:23', '2021-04-10 18:07:23', '2021-04-10 18:13:03', 0, 'Abbott', '7968662a5867559e72163e59911fb1ef'),
(63, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:08:03', '2021-04-10 18:08:03', '2021-04-10 18:08:15', 0, 'Abbott', '3db052a8f47784a7fac1d84b6adf2770'),
(64, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:08:46', '2021-04-10 18:08:46', '2021-04-10 18:09:16', 0, 'Abbott', '03d47ca1729636adbcf2001b2f823a16'),
(65, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:09:16', '2021-04-10 18:09:16', '2021-04-10 18:11:07', 0, 'Abbott', 'bae672cc46c4cf13bb5d98cb24af83f7'),
(66, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:09:20', '2021-04-10 18:09:20', '2021-04-10 18:09:27', 0, 'Abbott', '02a19b96deff665f583763318ae700cc'),
(67, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:09:36', '2021-04-10 18:09:36', '2021-04-10 18:10:00', 0, 'Abbott', 'feb3af4b0af3d54b8f1e2732b0d98d50'),
(68, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:10:09', '2021-04-10 18:10:09', '2021-04-10 18:13:20', 0, 'Abbott', 'b2bd06e962c670f3be112e83a892f37b'),
(69, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 18:10:32', '2021-04-10 18:10:32', '2021-04-10 18:11:16', 0, 'Abbott', '934b38af64672cd1f0c9549279cbb5ba'),
(70, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:10:46', '2021-04-10 18:10:46', '2021-04-10 18:10:55', 0, 'Abbott', '970033e69be88626d1b0187b8cb6e840'),
(71, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 18:10:47', '2021-04-10 18:10:47', '2021-04-10 18:13:51', 0, 'Abbott', '53fb96247dd0fa1db9aca7c9e83138cd'),
(72, 'anup taaksande', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', NULL, NULL, '2021-04-10 18:11:19', '2021-04-10 18:11:19', '2021-04-10 18:11:30', 0, 'Abbott', 'eb6e4879b9e59cd960a5742aa7a3b84c'),
(73, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:11:36', '2021-04-10 18:11:36', '2021-04-10 18:12:52', 0, 'Abbott', '5b26dad16d452a08dfc2ffd1405ed5ac'),
(74, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 18:11:41', '2021-04-10 18:11:41', '2021-04-10 18:12:27', 0, 'Abbott', 'c482c8743c15e601b5ab028c40ed0466'),
(75, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:11:42', '2021-04-10 18:11:42', '2021-04-10 18:12:05', 0, 'Abbott', '8e7f923c29f0abf90ddeeab4080b93fd'),
(76, 'anup taksande', 'anuprtaksande@gmail.com', 'mumbai', 'Wockhardt', NULL, NULL, '2021-04-10 18:12:17', '2021-04-10 18:12:17', '2021-04-10 18:12:24', 0, 'Abbott', '6e6957b96a156922ef4990897c17381a'),
(77, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 18:12:47', '2021-04-10 18:12:47', '2021-04-10 18:23:10', 0, 'Abbott', '964277b5f1415af4738b19b155c7619d'),
(78, 'Rahul Mahadik', 'rjmahadik@gmail.com', 'Sangli', 'Kulloli Hospital Sangli', NULL, NULL, '2021-04-10 18:12:48', '2021-04-10 18:12:48', '2021-04-10 18:19:58', 0, 'Abbott', '5d428091128dc25cbbe4e9b4d6d8efcb'),
(79, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:12:53', '2021-04-10 18:12:53', '2021-04-10 18:15:37', 0, 'Abbott', '07cb504fdb1b1a41f3e57a7d4dc58d13'),
(80, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:13:05', '2021-04-10 18:13:05', '2021-04-10 18:14:33', 0, 'Abbott', 'daf3f5cb7f3f093811047106ad1bddfc'),
(81, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:13:23', '2021-04-10 18:13:23', '2021-04-10 18:19:14', 0, 'Abbott', '14b17c65c46d089270eed89493788a90'),
(82, 'ANUP TAKSANDE', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', NULL, NULL, '2021-04-10 18:13:56', '2021-04-10 18:13:56', '2021-04-10 18:14:06', 0, 'Abbott', '9ed1868819c4e8f3d34c15b8b3c841a8'),
(83, 'Ashish', 'ashish.lohgaonkar@abbott.com', 'Kolhapur', 'Abbott', NULL, NULL, '2021-04-10 18:14:06', '2021-04-10 18:14:06', '2021-04-10 18:14:40', 0, 'Abbott', '2ea24a1958852b7118c2ffe81997c7b5'),
(84, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 18:14:14', '2021-04-10 18:14:14', '2021-04-10 18:14:23', 0, 'Abbott', '8378dd4f0a8c2f0544efad6f34fc14f4'),
(85, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:14:35', '2021-04-10 18:14:35', '2021-04-10 18:19:46', 0, 'Abbott', 'f3332ca6af307cf124eca226515c86e1'),
(86, 'ANUP TAKSANDE', 'anuprtaksande@gmail.com', 'mumbai', 'wockhardt', NULL, NULL, '2021-04-10 18:14:42', '2021-04-10 18:14:42', '2021-04-10 18:16:32', 0, 'Abbott', 'f7fe5522094cd73ac4e20fcce1ec225b'),
(87, 'Dr Dhiren Shah', 'drs1955@hotmail.com', 'MUMBAI', 'Saifee hospital', NULL, NULL, '2021-04-10 18:14:48', '2021-04-10 18:14:48', '2021-04-10 18:15:00', 0, 'Abbott', '1d38d7cd26d645179af3f3b924c96fc2'),
(88, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 18:14:52', '2021-04-10 18:14:52', '2021-04-10 18:15:27', 0, 'Abbott', '048b738bfc91682bd91d4e5432b567f7'),
(89, 'Dr Nazir juvale', 'drjuvale@rediffmail.com', 'MUMBAI', 'Saifee hospital', NULL, NULL, '2021-04-10 18:15:01', '2021-04-10 18:15:01', '2021-04-10 18:15:12', 0, 'Abbott', 'e793529c74c7dc2fb68b6ef05788c285'),
(90, 'Dr Yunus loya', 'yunusloya@gmail.com', 'MUMBAI', 'Prince aly khan hospital', NULL, NULL, '2021-04-10 18:15:13', '2021-04-10 18:15:13', '2021-04-10 18:15:25', 0, 'Abbott', 'c892bbebe5dab8b02023090754359a71'),
(91, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:15:25', '2021-04-10 18:15:25', '2021-04-10 18:18:56', 0, 'Abbott', '1bc98f42e4511daf555a026d65ec1704'),
(92, 'Joel', 'joelquadros11@gmail.com', 'Margao', 'Gmc', NULL, NULL, '2021-04-10 18:15:44', '2021-04-10 18:15:44', '2021-04-10 19:42:32', 0, 'Abbott', '130e877b88734821f3454f787f110b23'),
(93, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 18:16:03', '2021-04-10 18:16:03', '2021-04-10 18:18:46', 0, 'Abbott', '5e552287310d84ca5d2070cb899c888d'),
(94, 'Dr Alok Shah', 'ads1987@gmail.com', 'MUMBAI', 'Nair hospital', NULL, NULL, '2021-04-10 18:16:03', '2021-04-10 18:16:03', '2021-04-10 18:16:11', 0, 'Abbott', 'a490611378ef8a7a8d72bb8916ab8049'),
(95, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:16:11', '2021-04-10 18:16:11', '2021-04-10 18:18:31', 0, 'Abbott', 'fa5eeb0578a2a0e0af652e450812835e'),
(96, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:16:31', '2021-04-10 18:16:31', '2021-04-10 18:24:13', 0, 'Abbott', 'e3326cc20b29848630f78d345b5c2d4b'),
(97, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital ', NULL, NULL, '2021-04-10 18:17:01', '2021-04-10 18:17:01', '2021-04-10 18:21:48', 0, 'Abbott', 'f9ca64ed339241f351bf860fcffbc5bf'),
(98, 'Dr Kaushal Chattrapati', 'drkaushalc@gmail.com', 'MUMBAI', 'Saifee hospital', NULL, NULL, '2021-04-10 18:17:02', '2021-04-10 18:17:02', '2021-04-10 18:17:08', 0, 'Abbott', '12433aed3b42d8487569addea40c798d'),
(99, 'Ashish', 'ashish.lohgaonkar@abbott.com', 'Kolhapur', 'Abbott', NULL, NULL, '2021-04-10 18:17:18', '2021-04-10 18:17:18', '2021-04-10 18:17:34', 0, 'Abbott', '3927987cc99a9d58d319d995a9950d3a'),
(100, 'RAJEEV', 'DRKHARERAJEEV@GMAIL.COM', 'BARAMATI', 'DR KHARE CLINIC BARAMATI', NULL, NULL, '2021-04-10 18:18:38', '2021-04-10 18:18:38', '2021-04-10 18:25:29', 0, 'Abbott', '166ac5cd4668db4cf79c29a683e56b71'),
(101, 'Dr Girish kale', 'spandangirish4kale@gmail.com', 'Nashik', 'Sahyadri Hospital', NULL, NULL, '2021-04-10 18:18:41', '2021-04-10 18:18:41', '2021-04-10 18:19:07', 0, 'Abbott', 'f756bb0e503aca4ed4ac20c948b6eae5'),
(102, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 18:18:51', '2021-04-10 18:18:51', '2021-04-10 19:20:34', 0, 'Abbott', 'a054f79fa404559e02a24e95b93f2ee6'),
(103, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:19:17', '2021-04-10 18:19:17', '2021-04-10 18:42:11', 0, 'Abbott', '6208ce709e9188e99ecd08a87294e65d'),
(104, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:19:28', '2021-04-10 18:19:28', '2021-04-10 18:21:51', 0, 'Abbott', '0166628c1d673fb09e8819ff1f5d9286'),
(105, 'Rahul Mahadik', 'rjmahadik@gmail.com', 'Sangli', 'Kulloli Hospital Sangli', NULL, NULL, '2021-04-10 18:19:41', '2021-04-10 18:19:41', '2021-04-10 19:20:52', 0, 'Abbott', '49fe9000aae34323f68ae279c7b38b6a'),
(106, 'Anuj Khushalani', 'anuj.khushalani@abbott.com', 'Mumbai', 'ABC', NULL, NULL, '2021-04-10 18:19:43', '2021-04-10 18:19:43', '2021-04-10 18:21:19', 0, 'Abbott', '120e21fc67da734e5f61d4407df4df03'),
(107, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:19:54', '2021-04-10 18:19:54', '2021-04-10 18:20:04', 0, 'Abbott', '7ab2f43a1cc74a6766d45dead36ee885'),
(108, 'Krishnakumar G', 'krishnakumar.g@abbott.com', 'Mumbai', 'NA', NULL, NULL, '2021-04-10 18:20:02', '2021-04-10 18:20:02', '2021-04-10 18:37:04', 0, 'Abbott', '75f09fe66cf617b6506cd352e719e800'),
(109, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:20:09', '2021-04-10 18:20:09', '2021-04-10 18:22:44', 0, 'Abbott', '9a160f38a32c5362dfc01017e3df1733'),
(110, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:20:42', '2021-04-10 18:20:42', '2021-04-10 18:21:26', 0, 'Abbott', '89857caa62c798ed7ad8150872058bf8'),
(111, 'Dr Mahesh Aher', 'maheshaher16@gmail.com', 'nashik', 'Wockhardt', NULL, NULL, '2021-04-10 18:20:46', '2021-04-10 18:20:46', '2021-04-10 18:21:19', 0, 'Abbott', '8e8f965ac3c6d2783862af6609ae285f'),
(112, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 18:21:09', '2021-04-10 18:21:09', '2021-04-10 18:53:47', 0, 'Abbott', 'd7b638a8d1ba8d05ecc19afe1d4e0150'),
(113, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:21:41', '2021-04-10 18:21:41', '2021-04-10 18:27:16', 0, 'Abbott', 'b2743955ddd6b84cc20dd2067bbc07bc'),
(114, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:22:03', '2021-04-10 18:22:03', '2021-04-10 18:22:21', 0, 'Abbott', 'cb3c92fb0c8c39a01502fc4035efbb89'),
(115, 'Chirag Parikh', 'chiragcjp@gmail.com', 'Mumbai', 'Kikabhai', NULL, NULL, '2021-04-10 18:22:17', '2021-04-10 18:22:17', '2021-04-10 18:22:38', 0, 'Abbott', '9280fcb7e1108f63d6b25225b9e7b20c'),
(116, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:22:28', '2021-04-10 18:22:28', '2021-04-10 18:22:50', 0, 'Abbott', 'ca88699f18d25eb4e83e96a094afb488'),
(117, 'Ajay wagh', 'waghajay132@gmail.com', 'Mumbai', 'Surana hospital', NULL, NULL, '2021-04-10 18:22:56', '2021-04-10 18:22:56', '2021-04-10 18:25:02', 0, 'Abbott', 'ca05938b55bdc32b86f59402b5ee7574'),
(118, 'Prashant suresh pawar', 'drprashant8087@gmail.com', 'Karad', 'Sahyadri hospital, karad', NULL, NULL, '2021-04-10 18:23:11', '2021-04-10 18:23:11', '2021-04-10 18:33:31', 0, 'Abbott', '8668dc73e0885e1dd5ea5f816da1c4dc'),
(119, 'Sachin Adikari', 'sachin.adhikari@rfhospital.org', 'Mumbai', 'HN Reliance ', NULL, NULL, '2021-04-10 18:23:17', '2021-04-10 18:23:17', '2021-04-10 18:23:29', 0, 'Abbott', '792a1ee200dd23d06a5e0c224ddc7ea6'),
(120, 'Chirag Parikh', 'chiragcjp@gmail.com', 'Mumbai', 'Kikabhai', NULL, NULL, '2021-04-10 18:23:34', '2021-04-10 18:23:34', '2021-04-10 18:23:36', 0, 'Abbott', '130502d94718a01c3b73b179f80e1409'),
(121, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:23:50', '2021-04-10 18:23:50', '2021-04-10 18:23:59', 0, 'Abbott', '0942a95e494b896c18d9628124b23776'),
(122, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 18:23:52', '2021-04-10 18:23:52', '2021-04-10 19:06:05', 0, 'Abbott', 'e478590d2904cbf876fcab39085d03a8'),
(123, 'Chirag Parikh', 'chiragcjp@gmail.com', 'Mumbai', 'Kikabhai', NULL, NULL, '2021-04-10 18:23:57', '2021-04-10 18:23:57', '2021-04-10 18:24:12', 0, 'Abbott', '7d30d43570b7216dc0af7fea5866fee3'),
(124, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:24:03', '2021-04-10 18:24:03', '2021-04-10 18:24:08', 0, 'Abbott', '33bb60cd276d08d602a250f3fce035ad'),
(125, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:24:04', '2021-04-10 18:24:04', '2021-04-10 18:33:55', 0, 'Abbott', 'dd050f0b65ae24c0d5f3276bd5e02246'),
(126, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:24:15', '2021-04-10 18:24:15', '2021-04-10 18:24:39', 0, 'Abbott', '4edeb929542cbca7c47443c7f78135a6'),
(127, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:24:41', '2021-04-10 18:24:41', '2021-04-10 18:24:45', 0, 'Abbott', 'f9771b65f83ace2c68f2426140bb9681'),
(128, 'Dr A Sharma ', 'asharma1234@gmail.com', 'Mumbai ', 'JJ Hospital', NULL, NULL, '2021-04-10 18:25:56', '2021-04-10 18:25:56', '2021-04-10 19:14:01', 0, 'Abbott', 'b40d4f355932b2a3abb73a41c5cda680'),
(129, 'Chirag Parikh', 'chiragcjp@gmail.com', 'Mumbai', 'Kikabhai', NULL, NULL, '2021-04-10 18:26:43', '2021-04-10 18:26:43', '2021-04-10 18:27:02', 0, 'Abbott', '8cf831155091f82b11f68612c32245b1'),
(130, 'Anuj Khushalani', 'anuj.khushalani@abbott.com', 'Mumbai', 'ABC', NULL, NULL, '2021-04-10 18:26:56', '2021-04-10 18:26:56', '2021-04-10 18:27:57', 0, 'Abbott', 'e90a29d00b400c89d9c3674f3a7d06c0'),
(131, 'Dr Nirmal Kolte', 'nirmalkolte@gmail.com', 'nasik', 'six sigma', NULL, NULL, '2021-04-10 18:27:46', '2021-04-10 18:27:46', '2021-04-10 18:27:49', 0, 'Abbott', '349528ee7a72ae244bbd249e697d2a7f'),
(132, 'Chirag Parikh', 'chiragcjp@gmail.com', 'Mumbai', 'Kikabhai', NULL, NULL, '2021-04-10 18:27:47', '2021-04-10 18:27:47', '2021-04-10 18:28:19', 0, 'Abbott', '2e8b6b0bddb5801822fe05e754513d96'),
(133, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:27:52', '2021-04-10 18:27:52', '2021-04-10 18:27:54', 0, 'Abbott', '8a85ffd4ea9f982dcd56d1569a37edbc'),
(134, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:27:57', '2021-04-10 18:27:57', '2021-04-10 18:31:02', 0, 'Abbott', '3dad27441cc5e7f08e79c42391725831'),
(135, 'Dr Girish kale', 'spandangirish4kale@gmail.com', 'Nashik', 'Sahyadri Hospital', NULL, NULL, '2021-04-10 18:28:01', '2021-04-10 18:28:01', '2021-04-10 18:28:07', 0, 'Abbott', '6c3529247b5ceb87a2b86fe986cc521b'),
(136, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:28:09', '2021-04-10 18:28:09', '2021-04-10 18:30:13', 0, 'Abbott', '722db42d342e72cd5cd5741bd90a5810'),
(137, 'Nitin Bote ', 'nnbote85@gmail.com', 'Mumbai', 'Somaiya ', NULL, NULL, '2021-04-10 18:28:43', '2021-04-10 18:28:43', '2021-04-10 18:33:20', 0, 'Abbott', '921036ff7ce807303bce64087ec384e6'),
(138, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:29:26', '2021-04-10 18:29:26', '2021-04-10 18:29:41', 0, 'Abbott', '72885614aafda0c2868543140d7bb05f'),
(139, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:30:37', '2021-04-10 18:30:37', '2021-04-10 18:30:47', 0, 'Abbott', '1fe0e2b22a282374a9a062545433b845'),
(140, 'Pooja', 'test@test.com', 'Test', 'Test', NULL, NULL, '2021-04-10 18:30:57', '2021-04-10 18:30:57', '2021-04-10 18:31:22', 0, 'Abbott', '54de1fa0f7fc93a59eb07683fb94bf69'),
(141, 'Dr Chetan Bhambure', 'cubhambure@gmail.com', 'MUMBAI', 'Wockhardt hospital', NULL, NULL, '2021-04-10 18:31:00', '2021-04-10 18:31:00', '2021-04-10 18:31:06', 0, 'Abbott', '3f563b36821b670d605affb11bf4e8b3'),
(142, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 18:31:16', '2021-04-10 18:31:16', '2021-04-10 19:18:20', 0, 'Abbott', '057c8daf30be5a45bc6c46295683da61'),
(143, 'Dr Naeem Fatta', 'naeem.fatta@gmail.com', 'MUMBAI', 'Masina hospital', NULL, NULL, '2021-04-10 18:31:33', '2021-04-10 18:31:33', '2021-04-10 18:31:42', 0, 'Abbott', 'b90767a1bde672a9ca0a61d908470eed'),
(144, 'Arun Bade', 'dr.arunbade@gmail.com', 'Beed', 'Abbott', NULL, NULL, '2021-04-10 18:31:37', '2021-04-10 18:31:37', '2021-04-10 18:31:41', 0, 'Abbott', '59bc3300acbcf24fd823ea9eb26196b8'),
(145, 'Arun Bade', 'dr.arunbade@gmail.com', 'Beed', 'Shivajirao Heart care', NULL, NULL, '2021-04-10 18:32:19', '2021-04-10 18:32:19', '2021-04-10 18:36:15', 0, 'Abbott', '2bf4edf52d0f4f1ddedc7ba0141b6601'),
(146, 'Dr Amit Patil', 'patilamit191@gmail.com', 'MUMBAI', 'Wockhardt hospital', NULL, NULL, '2021-04-10 18:32:30', '2021-04-10 18:32:30', '2021-04-10 18:32:33', 0, 'Abbott', '8d129dee8878423f53723cdf6500f804'),
(147, 'Dr Ankur Phatarpekar', 'drankurmd@gmail.com', 'MUMBAI', 'Wockhardt hospital', NULL, NULL, '2021-04-10 18:33:13', '2021-04-10 18:33:13', '2021-04-10 18:33:18', 0, 'Abbott', '22ff5ee4cc0ebee96d334885d296e8bf'),
(148, 'Prashant suresh pawar', 'drprashant8087@gmail.com', 'Karad', 'Sahyadri hospital, karad', NULL, NULL, '2021-04-10 18:33:38', '2021-04-10 18:33:38', '2021-04-10 18:35:38', 0, 'Abbott', '898303ed7cc4cffd068fd7850e20078b'),
(149, 'Dr Vaibhav Dedhia', 'dedhiadoc@gmail.com', 'MUMBAI', 'Prince aly khan hospital', NULL, NULL, '2021-04-10 18:33:50', '2021-04-10 18:33:50', '2021-04-10 18:33:57', 0, 'Abbott', '08b59f9fcebebc1b3b0125f053e41161'),
(150, 'Sameer Pagad', 'spagad@yahoo.com', 'Mumbai', 'Somaiya ', NULL, NULL, '2021-04-10 18:34:23', '2021-04-10 18:34:23', '2021-04-10 20:11:03', 0, 'Abbott', '6c6e29c26837cc4edcaf0d1120b061bb'),
(151, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:38:33', '2021-04-10 18:38:33', '2021-04-10 18:38:48', 0, 'Abbott', 'cae879cf7bb7e6231648bafc5e9e59ea'),
(152, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:39:18', '2021-04-10 18:39:18', '2021-04-10 18:47:23', 0, 'Abbott', 'bfbfd808a616f0b97f063433be11f8fc'),
(153, 'Dr Abhishek', 'abhidoctor@yahoo.com', 'Mumbai', 'Ltmg Hospital', NULL, NULL, '2021-04-10 18:39:30', '2021-04-10 18:39:30', '2021-04-10 18:39:34', 0, 'Abbott', '024b3f4dd718a4af653bd302469d2c21'),
(154, 'Dr Abhishek', 'abhidoctor@yahoo.com', 'Mumbai', 'Ltmg Hospital', NULL, NULL, '2021-04-10 18:39:41', '2021-04-10 18:39:41', '2021-04-10 20:15:02', 0, 'Abbott', '54a8bc393e3507af73716723377ec3d6'),
(155, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 18:41:50', '2021-04-10 18:41:50', '2021-04-10 18:57:41', 0, 'Abbott', '3553108cc6beb5526d41e0ad65b9157b'),
(156, 'dr aniket gadre', 'draniketgadre@gmail.com', 'pune', 'DMH', NULL, NULL, '2021-04-10 18:43:21', '2021-04-10 18:43:21', '2021-04-10 20:03:49', 0, 'Abbott', 'e51387ccce82a53098facc45947195eb'),
(157, 'Ramkrishna Sundaram', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-04-10 18:43:51', '2021-04-10 18:43:51', '2021-04-10 20:03:55', 0, 'Abbott', 'e0d306beee14803d301670b66fffa6b0'),
(158, 'Badri vetal', 'badrivetal@gmail.com', 'Mumbai', 'Surana', NULL, NULL, '2021-04-10 18:46:50', '2021-04-10 18:46:50', '2021-04-10 18:47:46', 0, 'Abbott', '6fcf05ef2030b47c001f22876f3b4ec2'),
(159, 'Sadanand Shetty', 'sadanantshetty54@gmail.com', 'Mumbai', 'Somaiya ', NULL, NULL, '2021-04-10 18:46:52', '2021-04-10 18:46:52', '2021-04-10 18:46:57', 0, 'Abbott', '45e63366af3e09833ce35a6c72d8542c'),
(160, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:47:38', '2021-04-10 18:47:38', '2021-04-10 18:47:55', 0, 'Abbott', 'b59a2a5b2621df571bc83783aa71328c'),
(161, 'Badri vetal', 'badrivetal@gmail.com', 'Mumbai', 'Surana', NULL, NULL, '2021-04-10 18:48:05', '2021-04-10 18:48:05', '2021-04-10 19:23:11', 0, 'Abbott', 'd2866117b9b3df33c833184fe0bd00bd'),
(162, 'Shantanu', 'drshantanud@yahoo.com', 'mumbai', 'jjh', NULL, NULL, '2021-04-10 18:48:31', '2021-04-10 18:48:31', '2021-04-10 18:49:46', 0, 'Abbott', 'e565feaf0c0d541a940f2d89e1a743dc'),
(163, 'Shantanu', 'drshantanud@yahoo.com', 'mumbai', 'jjh', NULL, NULL, '2021-04-10 18:51:49', '2021-04-10 18:51:49', '2021-04-10 18:51:58', 0, 'Abbott', 'bf1b88a16e4a61f4a88d4e8fb964be81'),
(164, 'Dr Sachin gavade', 'drsachingavade20@gmail.com', 'Sangli', 'Bharati', NULL, NULL, '2021-04-10 18:51:54', '2021-04-10 18:51:54', '2021-04-10 19:10:42', 0, 'Abbott', '15d7a431b80458d13650c954dded7e97'),
(165, 'Sachin ', 'sanapsachin1986@gmail.com', 'Sangli ', 'Sanap ', NULL, NULL, '2021-04-10 18:54:18', '2021-04-10 18:54:18', '2021-04-10 19:13:33', 0, 'Abbott', 'fa543df5e0811c6c67d349d09b7e4f0d'),
(166, 'Dr mahipat ', 'mahipat_soni@yahoo.co.in', 'Sriganganagar ', 'Gaur hospital ', NULL, NULL, '2021-04-10 18:54:56', '2021-04-10 18:54:56', '2021-04-10 18:55:26', 0, 'Abbott', '7bab26ff5b7b57765792dd88ee6ea2a9'),
(167, 'Dr mahipat ', 'mahipat_soni@yahoo.co.in', 'Sriganganagar ', 'Gaur hospital ', NULL, NULL, '2021-04-10 18:56:01', '2021-04-10 18:56:01', '2021-04-10 19:09:09', 0, 'Abbott', '167dd6db391f16a91ae17bb187c69aa4'),
(168, 'Dr.Sachin Chaudhary', 'schaudhary1983@gmail.com', 'Mumbai', 'Fortis', NULL, NULL, '2021-04-10 18:57:07', '2021-04-10 18:57:07', '2021-04-10 18:58:54', 0, 'Abbott', '7177296872978d5a3547e2a01fecbd99'),
(169, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 18:57:36', '2021-04-10 18:57:36', '2021-04-10 19:09:15', 0, 'Abbott', '4ce5b3c7410def26a0df7b1fdd1154d9'),
(170, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:00:25', '2021-04-10 19:00:25', '2021-04-10 19:01:20', 0, 'Abbott', '1e34e0b2a77dc05bbc322e2a4e0152d4'),
(171, 'V', 'v@ta.c', 'gf', 'h', NULL, NULL, '2021-04-10 19:01:07', '2021-04-10 19:01:07', '2021-04-10 19:01:12', 0, 'Abbott', '884ea6156889f95e8a907c1ec99b1451'),
(172, 'V', 'v@ta.c', 'gf', 'h', NULL, NULL, '2021-04-10 19:01:18', '2021-04-10 19:01:18', '2021-04-10 19:01:21', 0, 'Abbott', '0a7a2e7a6325764a1928dc6213363342'),
(173, 'V', 'v@ta.c', 'gf', 'h', NULL, NULL, '2021-04-10 19:01:39', '2021-04-10 19:01:39', '2021-04-10 19:01:42', 0, 'Abbott', '622b04f2c54a4cd280c0affca8ac092d'),
(174, 'V', 'v@ta.c', 'gf', 'h', NULL, NULL, '2021-04-10 19:01:58', '2021-04-10 19:01:58', '2021-04-10 20:28:04', 0, 'Abbott', '6f388d4a2408f66d2d73e10b350e7e85'),
(175, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:02:37', '2021-04-10 19:02:37', '2021-04-10 19:03:13', 0, 'Abbott', '372f8c0b3069cdc1aff3f00b406b374d'),
(176, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:03:37', '2021-04-10 19:03:37', '2021-04-10 19:04:19', 0, 'Abbott', '1ca13e5a807a30285b95058e633d15f0'),
(177, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:06:04', '2021-04-10 19:06:04', '2021-04-10 19:27:25', 0, 'Abbott', '597433ceef155c76d1f1f1b401696fed'),
(178, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:07:23', '2021-04-10 19:07:23', '2021-04-10 19:08:03', 0, 'Abbott', '697e40032f3f4c76888c1815a9ea7620'),
(179, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 19:07:24', '2021-04-10 19:07:24', '2021-04-10 19:21:08', 0, 'Abbott', '2b142ea1efeac47590fa4fcc40526bb8'),
(180, 'PRAVIN', 'pravinshenkar40@gmail.com', 'MUMBAI', 'Kda hospital', NULL, NULL, '2021-04-10 19:08:16', '2021-04-10 19:08:16', '2021-04-10 19:10:27', 0, 'Abbott', '2d72cbfb2f644f92ab6064c1c08f6dd9'),
(181, 'Dr mahipat ', 'mahipat_soni@yahoo.co.in', 'Sriganganagar ', 'Gaur hospital', NULL, NULL, '2021-04-10 19:10:01', '2021-04-10 19:10:01', '2021-04-10 19:11:30', 0, 'Abbott', 'fe7ce6a9a3547b977d9ebcd86df1828e'),
(182, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:11:17', '2021-04-10 19:11:17', '2021-04-10 19:15:07', 0, 'Abbott', '24e5a06ddd45cf54400925245b6877a4'),
(183, 'Dr mahipat ', 'mahipat_soni@yahoo.co.in', 'Sriganganagar ', 'Gaur hospital', NULL, NULL, '2021-04-10 19:12:12', '2021-04-10 19:12:12', '2021-04-10 19:15:07', 0, 'Abbott', 'c3ead4121d19435cad6455d984a15149'),
(184, 'Dr Sachin gavade', 'drsachingavade20@gmail.com', 'Sangli', 'Bharati', NULL, NULL, '2021-04-10 19:13:22', '2021-04-10 19:13:22', '2021-04-10 19:20:02', 0, 'Abbott', '8671e1a62eabe64f34c6d93da87ca42b'),
(185, 'Sachin ', 'sanapsachin1986@gmail.com', 'Sangli ', 'Sanap', NULL, NULL, '2021-04-10 19:13:39', '2021-04-10 19:13:39', '2021-04-10 19:21:34', 0, 'Abbott', '02d310121344b5e1f39f3d6122467482'),
(186, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:15:40', '2021-04-10 19:15:40', '2021-04-10 19:19:50', 0, 'Abbott', 'c8e13e6250bba4ff8377c09ba6a66f52'),
(187, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'AV', NULL, NULL, '2021-04-10 19:15:43', '2021-04-10 19:15:43', '2021-04-10 19:16:15', 0, 'Abbott', 'e8d856003ba3fa637470835cd711c304'),
(188, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 19:18:28', '2021-04-10 19:18:28', '2021-04-10 19:51:12', 0, 'Abbott', '05aef82e95136f5c3dd6d79b4a275d46'),
(189, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 19:18:38', '2021-04-10 19:18:38', '2021-04-10 19:20:46', 0, 'Abbott', '3b0f44b2267d37ace12c953ce80f4e81'),
(190, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:19:24', '2021-04-10 19:19:24', '2021-04-10 19:20:14', 0, 'Abbott', 'f34d27ca10e5575459db8a947be53d30'),
(191, 'Dr Sachin gavade', 'drsachingavade20@gmail.com', 'Sangli', 'Bharati', NULL, NULL, '2021-04-10 19:19:36', '2021-04-10 19:19:36', '2021-04-10 19:23:22', 0, 'Abbott', '7b37394f3acf6515dfd4b9aaff68523f'),
(192, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:19:50', '2021-04-10 19:19:50', '2021-04-10 19:20:11', 0, 'Abbott', '1a7f379a87ed606e3b61caef2026c8c6'),
(193, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:20:15', '2021-04-10 19:20:15', '2021-04-10 19:20:39', 0, 'Abbott', 'eaf4767590adc2d10d8ad1ea2e68b6b2'),
(194, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:20:44', '2021-04-10 19:20:44', '2021-04-10 19:20:56', 0, 'Abbott', '4ca9461e9db03ae55baa0dff80891d80'),
(195, 'Udit Mathur', 'udit.mathur144@jtb-india.com', 'Test', 'JTBTest', NULL, NULL, '2021-04-10 19:21:18', '2021-04-10 19:21:18', '2021-04-10 19:24:19', 0, 'Abbott', 'bc3cab70c608a33b254b42c940edb6c2'),
(196, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:21:24', '2021-04-10 19:21:24', '2021-04-10 19:28:02', 0, 'Abbott', 'd3910bafe301b8d7b91883e71bedab39'),
(197, 'Sachin ', 'sanapsachin1986@gmail.com', 'Sangli ', 'Sanap', NULL, NULL, '2021-04-10 19:21:38', '2021-04-10 19:21:38', '2021-04-10 19:31:28', 0, 'Abbott', '3608567771bf5b85b59163e0e9c811a9'),
(198, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 19:21:40', '2021-04-10 19:21:40', '2021-04-10 19:33:16', 0, 'Abbott', 'f67386939a85f64600213d25f441f734'),
(199, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:22:00', '2021-04-10 19:22:00', '2021-04-10 19:22:08', 0, 'Abbott', 'ba243c32fbd1ea707d573c64ffeea40d'),
(200, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:22:16', '2021-04-10 19:22:16', '2021-04-10 19:22:49', 0, 'Abbott', '84e351a7ddaa07eff86b727048fcd641'),
(201, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:23:09', '2021-04-10 19:23:09', '2021-04-10 19:23:33', 0, 'Abbott', '068b3e1a509791ef1e20982b31408a9b'),
(202, 'Dr Sachin gavade', 'drsachingavade20@gmail.com', 'Sangli', 'Bharati', NULL, NULL, '2021-04-10 19:23:28', '2021-04-10 19:23:28', '2021-04-10 19:27:26', 0, 'Abbott', '181b2b4684129ddeb2eeeead0709b896'),
(203, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:23:37', '2021-04-10 19:23:37', '2021-04-10 19:26:24', 0, 'Abbott', '8881377af0edf6e6444a7c8d284ff5ea'),
(204, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:23:42', '2021-04-10 19:23:42', '2021-04-10 19:24:47', 0, 'Abbott', 'c09f5b9fb4735650767b36aaa38173b0'),
(205, 'Udit', 'udit.mathur144@jtb-india.com', 'Delhi', 'Test', NULL, NULL, '2021-04-10 19:25:30', '2021-04-10 19:25:30', '2021-04-10 19:27:05', 0, 'Abbott', '140b66ae02bea99066433dae94d3a0a3'),
(206, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:25:44', '2021-04-10 19:25:44', '2021-04-10 19:25:49', 0, 'Abbott', 'a0e844f316b578af71c63823f3fbe859'),
(207, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:25:54', '2021-04-10 19:25:54', '2021-04-10 19:32:56', 0, 'Abbott', 'e45dc8cabd2a5f2d866ace9992e28b4a'),
(208, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:26:29', '2021-04-10 19:26:29', '2021-04-10 19:26:50', 0, 'Abbott', 'f0b90d8f2c7d0af9623d75f39c99f5ce'),
(209, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:27:14', '2021-04-10 19:27:14', '2021-04-10 19:34:09', 0, 'Abbott', '91d7a9abe2df61f2d8a6ed929d7ce5d3'),
(210, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 19:27:33', '2021-04-10 19:27:33', '2021-04-10 19:40:19', 0, 'Abbott', '9ccb5578286db5a876a258c0de6b077b'),
(211, 'Udit Mathur', 'udit.mathur144@jtb-india.com', 'Test', 'JTBTest', NULL, NULL, '2021-04-10 19:27:51', '2021-04-10 19:27:51', '2021-04-10 19:43:50', 0, 'Abbott', '0ccd04c4c5d57404cb8fd4cf175b71fd'),
(212, 'Badri vetal', 'badrivetal@gmail.com', 'Mumbai', 'Surana', NULL, NULL, '2021-04-10 19:29:41', '2021-04-10 19:29:41', '2021-04-10 19:35:26', 0, 'Abbott', '1c48bbad243a5154880db902aba5948d'),
(213, 'Deorao', 'cardioatapex@gmail.com', 'Aurangabad', 'Asian', NULL, NULL, '2021-04-10 19:32:51', '2021-04-10 19:32:51', '2021-04-10 20:02:18', 0, 'Abbott', 'f2ca1bf8639542f25ce8955dde3bde99'),
(214, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:34:11', '2021-04-10 19:34:11', '2021-04-10 19:37:16', 0, 'Abbott', 'ed7264581fc7e7093f6242ce51ff1499'),
(215, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:34:22', '2021-04-10 19:34:22', '2021-04-10 19:35:08', 0, 'Abbott', '3444406dd96082c35bb544ae2447d257'),
(216, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 19:35:49', '2021-04-10 19:35:49', '2021-04-10 19:41:40', 0, 'Abbott', '51459516c42dfa86406c4da53f33f0d1'),
(217, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:36:08', '2021-04-10 19:36:08', '2021-04-10 19:58:31', 0, 'Abbott', '6a6f6397c9bb2dc51cb0c3be928453d9'),
(218, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:37:27', '2021-04-10 19:37:27', '2021-04-10 19:40:57', 0, 'Abbott', '074f1e150f6263a5544526e7c844929c'),
(219, 'Amal', 'amal.vyas@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-04-10 19:40:11', '2021-04-10 19:40:11', '2021-04-10 19:40:39', 0, 'Abbott', 'a79283926617fbb050bd8c467803d057'),
(220, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 19:40:39', '2021-04-10 19:40:39', '2021-04-10 19:40:55', 0, 'Abbott', '299046066a9e9143d90ee91da1c5dd05'),
(221, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 19:42:06', '2021-04-10 19:42:06', '2021-04-10 20:04:37', 0, 'Abbott', '5a75a9427f328015d1211f453dc7a234'),
(222, 'nauman Shaikh', 'shaikhnauman417@gmail.com', 'MUMBAI', 'Sushrut hospital ', NULL, NULL, '2021-04-10 19:42:17', '2021-04-10 19:42:17', '2021-04-10 19:43:09', 0, 'Abbott', '4fd3dd559576dd9cbdb9860aa97c5990'),
(223, 'Shrikant Patil', 'shrikant.dagdupatil@abbott.com', 'Nashik', 'Abbott', NULL, NULL, '2021-04-10 19:51:20', '2021-04-10 19:51:20', '2021-04-10 20:23:41', 0, 'Abbott', 'ed19cf3f9f9d84d09c5257fcdf40c71e'),
(224, 'Shrikant N. Chaudhari ', 'shitushri137@gmail.com', 'Mumbai', 'Surana Sethia Hospital And Research Center', NULL, NULL, '2021-04-10 19:52:15', '2021-04-10 19:52:15', '2021-04-10 20:00:20', 0, 'Abbott', 'b1325c6a9801ba776df7f086b6016410'),
(225, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:58:49', '2021-04-10 19:58:49', '2021-04-10 19:58:57', 0, 'Abbott', 'b3eedfdc0d4fb2ea62b8d38630bc18de'),
(226, 'Ajeya Mundhekar', 'ajeya_m@rediffmail.com', 'GOA', 'Healthway', NULL, NULL, '2021-04-10 19:59:25', '2021-04-10 19:59:25', '2021-04-10 20:03:56', 0, 'Abbott', 'c94878fcc87137c6d8bf745707326152'),
(227, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'Abbott Vascular', NULL, NULL, '2021-04-10 20:02:04', '2021-04-10 20:02:04', '2021-04-10 20:02:26', 0, 'Abbott', '4ca45ef5adcb6782abbb1da4d6e0d333'),
(228, 'Umesh khedkar', 'dr.umeshkhedkar@gmail.com', 'Aurangabad', 'United ciigma hospital', NULL, NULL, '2021-04-10 20:04:56', '2021-04-10 20:04:56', '2021-04-10 20:06:16', 0, 'Abbott', 'f49f986225381a332f420332bf297c35'),
(229, 'Udit Mathur', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-04-10 20:06:43', '2021-04-10 20:06:43', '2021-04-10 20:07:34', 0, 'Abbott', '3355b72d978c59c4b3e9e91977c57617'),
(230, 'Dr Sachin gavade', 'drsachingavade20@gmail.com', 'Sangli', 'Bharati', NULL, NULL, '2021-04-10 20:15:26', '2021-04-10 20:15:26', '2021-04-10 20:15:34', 0, 'Abbott', 'f97b45b569f0a779a76e24949bf53082'),
(231, 'KUMAR RAJEEV', 'doc_rajeev@rediffmail.com', 'navi mumbai', 'Fortis ', NULL, NULL, '2021-04-10 20:47:55', '2021-04-10 20:47:55', '2021-04-10 20:48:45', 0, 'Abbott', '29f423a8b8d4ed83558e73fe7da2195a'),
(232, 'KUMAR RAJEEV', 'doc_rajeev@rediffmail.com', 'navi mumbai', 'Fortis ', NULL, NULL, '2021-04-10 20:49:32', '2021-04-10 20:49:32', '2021-04-10 20:50:02', 0, 'Abbott', 'abdfd8d460f931999c42593404056423'),
(233, 'Uday', 'Hement08@Rediffmail.com', 'Kolhapur', 'Sharanya', NULL, NULL, '2021-04-11 19:25:01', '2021-04-11 19:25:01', '2021-04-11 19:25:15', 0, 'Abbott', '5a89ae8c25d04e4c3b593cb1d00898e8'),
(234, 'Dr Amit sharma', 'amitthedoc@hotmail.com', 'Mumbai', 'Holy spirit', NULL, NULL, '2021-04-13 18:37:11', '2021-04-13 18:37:11', '2021-04-13 18:38:45', 0, 'Abbott', '90c1d88e70e196e9024c36626c8b6825');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
